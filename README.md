# What is this?

Gaming Borderless is a modified version of Borderless Gaming. The base of the code is from [CodeUSA](https://github.com/Codeusa). I'm not trying to take credit for his work. The source code repository is on GitHub at [Codeusa/Borderless-Gaming](https://github.com/Codeusa/Borderless-Gaming).

It is a simple tool that will allow you to turn your windowed video games into "fullscreen" applications without all of the negative side effects.

The project is open source under the [GNU General Public License v2.0](https://gitlab.com/aDigitalPhantom/GamingBorderless/blob/master/LICENSE).

## How to install and run GamingBorderless

#### Install from Source
- Clone the repository 
- Open the solution with Visual Studio 2017
- Disable signing, or generate a new key to sign with
- Select the "Release" option 
- Build the projects and use the generated executable

### Install and run
Download the latest build at [RandomGamers.org](https://randomgamers.org/showthread.php?tid=26)

#### Need help?
The software is provided as-is.


## Helping GamingBorderless 
Until it is decided how to have this done, contributions will not be accepted.
